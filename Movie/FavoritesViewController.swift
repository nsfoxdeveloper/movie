//
//  FavoritesViewController.swift
//  Movie
//
//  Created by Юрий Дурнев on 14.07.16.
//  Copyright © 2016 example. All rights reserved.
//

import UIKit
import ObjectMapper
import Async
import RealmSwift

class FavoritesViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var series = [SeriesById]()
    var isEditingMode = false

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(animated: Bool) {
        Async.main {
            self.series.removeAll()
            }.background {
            
            if let userFav = UserOperation().user()?.favoritesList {
                for item in userFav {
                    if let object = Mapper<SeriesById>().map(item.objectString) {
                        self.series.append(object)
                    }
                }
            }
            }.main {
               self.tableView.reloadData()
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showFavSeries" {
            let vc = segue.destinationViewController as! SeriesDetailViewController
            if let series = sender as? SeriesById {
                vc.showSeries(series)
            }
        }
    }
    
    @IBAction func editFavorites(sender: UIBarButtonItem) {
        isEditingMode = !isEditingMode
        self.tableView.setEditing(isEditingMode, animated: true)
    }
}

extension FavoritesViewController: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.series.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! SeriesSearchCell
        if let series: SeriesById = self.series[indexPath.row] {
                cell.setData(series)
        }
        return cell
    }

}

extension FavoritesViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if let series: SeriesById = self.series[indexPath.row] {
            self.performSegueWithIdentifier("showFavSeries", sender: series)
        }
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: UITableViewRowActionStyle.Destructive, title: "Delete") { (deleteAction, indexPath) -> Void in
            if let userFav = UserOperation().user()?.favoritesList {
                if let fav: Favorites = userFav[indexPath.row] {
                    let realm = try! Realm()
                    try! realm.write {
                         realm.delete(fav)
                    }
                    self.series.removeAtIndex(indexPath.row)
                    tableView.beginUpdates()
                    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
                    tableView.endUpdates()
                }
            }
        }
        
        return [deleteAction]
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 165
    }
}

