//
//  AppDelegate.swift
//  Movie
//
//  Created by Юрий Дурнев on 05.07.16.
//  Copyright © 2016 example. All rights reserved.
//

import UIKit
import VK_ios_sdk
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit
import Fabric
import TwitterKit
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        let titleDict = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        UINavigationBar.appearance().titleTextAttributes = titleDict
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        UINavigationBar.appearance().barTintColor = UIColor.blackColor()
        
        UITabBar.appearance().tintColor = UIColor.whiteColor()
        UITabBar.appearance().barTintColor = UIColor.blackColor()

        print(Keychaine.sharedInstance.getUserToken())
        let userDefaults = NSUserDefaults.standardUserDefaults()
        if userDefaults.boolForKey("FirstRun") == false {
            Keychaine.sharedInstance.removeUserToken({
                userDefaults.setBool(true, forKey: "FirstRun")
                userDefaults.synchronize()
            })
        }
        self.socialNetwork(application, launchOptions: launchOptions)
        return true
    }
    
    //MARK: - Social
    func socialNetwork(application: UIApplication, launchOptions: [NSObject: AnyObject]?) {
        //Facebook
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        FBSDKLoginManager.renewSystemCredentials { (result, error) in }
        //Vk
        VKSdk.initializeWithAppId("5535157")
        //twitter & fabric
        Fabric.with([Crashlytics.self, Twitter.self])
    }
    
    @available(iOS 9, *)
    func application(app: UIApplication, openURL url: NSURL, options: [String : AnyObject]) -> Bool {
        FBSDKApplicationDelegate.sharedInstance().application(app, openURL: url,
                                                              sourceApplication: options[UIApplicationOpenURLOptionsSourceApplicationKey] as! String?,
                                                              annotation: options[UIApplicationOpenURLOptionsAnnotationKey])
        VKSdk.processOpenURL(url, fromApplication: options[UIApplicationOpenURLOptionsSourceApplicationKey] as! String)
        return true
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
        VKSdk.processOpenURL(url, fromApplication: sourceApplication)
        return true
    }

    //MARK: - Application state
    func applicationWillResignActive(application: UIApplication) {}

    func applicationDidEnterBackground(application: UIApplication) {}

    func applicationWillEnterForeground(application: UIApplication) {}

    func applicationDidBecomeActive(application: UIApplication) {
        FBSDKAppEvents.activateApp()
    }

    func applicationWillTerminate(application: UIApplication) {}
    
    func getCurrentViewController(vc: UIViewController) -> UIViewController? {
        if let pvc = vc.presentedViewController {
            return getCurrentViewController(pvc)
        }
        else if let svc = vc as? UISplitViewController where svc.viewControllers.count > 0 {
            return getCurrentViewController(svc.viewControllers.last!)
        }
        else if let nc = vc as? UINavigationController where nc.viewControllers.count > 0 {
            return getCurrentViewController(nc.topViewController!)
        }
        else if let tbc = vc as? UITabBarController {
            if let svc = tbc.selectedViewController {
                return getCurrentViewController(svc)
            }
        }
        return vc
    }

}

