//
//  DetailHeaderView.swift
//  Movie
//
//  Created by Юрий Дурнев on 07.07.16.
//  Copyright © 2016 example. All rights reserved.
//

import UIKit
import GSKStretchyHeaderView

class DetailHeaderView: GSKStretchyHeaderView {
     @IBOutlet weak var headerImage: UIImageView!
     @IBOutlet weak var headerTitle: UILabel!
     @IBOutlet weak var headerRating: UILabel!
     @IBOutlet weak var headerEpisodes: UILabel!
    
    func setData(series: SeriesById) {
        if let title = series.seriesName {
            self.headerTitle.text = title
        }
        
        if let rating = series.rating {
            self.headerRating.text = rating
        }
        
        if let banner = series.banner where banner != "" {
            let URL = NSURL(string: "http://thetvdb.com/banners/\(banner)")!
            self.headerImage.sd_setImageWithURL(URL, placeholderImage: UIImage(named: "placeholder-search"))
        }
    }
}
