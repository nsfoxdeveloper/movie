//
//  User.swift
//  Movie
//
//  Created by Юрий Дурнев on 14.07.16.
//  Copyright © 2016 example. All rights reserved.
//

import Foundation
import RealmSwift

class User: Object {
    
    dynamic var userId: String?
    dynamic var isLogin = false
    var favoritesList = List<Favorites>()
    
    override static func primaryKey() -> String? {
        return "userId"
    }
}

class Favorites: Object {
    dynamic var favId = 0
    dynamic var objectString: String?
}

struct UserOperation {
    func user() -> User? {
        if let user = try? Realm().objects(User).filter("isLogin == true").first {
            return user!
        }
        return nil
    }
    
    func logoutUser() {
        if let user = try? Realm().objects(User).filter("isLogin == true").first {
            try! Realm().write {
               user?.isLogin = false
            }
        }
    }
    
    func userOperation(write: ((user: User?) -> Void)?) {
        try! Realm().write {
            write?(user: user())
        }
    }
}
