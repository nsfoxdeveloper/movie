//
//  SeriesViewController.swift
//  Movie
//
//  Created by Юрий Дурнев on 06.07.16.
//  Copyright © 2016 example. All rights reserved.
//

import UIKit
import Async
import SDWebImage

class SeriesViewController: UITableViewController, UISearchBarDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    
    var series = [Series]()
    var updateSeries = [String: [UpdateModel]]()
    var sectionSorting = [String]()
    var search = false
    var tap: UITapGestureRecognizer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        self.getUpdateSeries()
    }
    
    func getUpdateSeries() {
        ApiClient.sharedInstance.seriesUpdate({ [weak self] (update) in
            self?.searchByDate(update)
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    func dismissKeyboard(){
        self.searchBar.resignFirstResponder()
        if let tapGest = self.tap {
            self.view.removeGestureRecognizer(tapGest)
        }
    }
    
    func searchByDate(update: [UpdateModel]) {
        Async.background {
            var sections = [String: [UpdateModel]]()
            
            for item in update {
                if let date = item.lastUpdated?.toString(format: .Custom("dd MMMM yyyy")) {
                    if sections.indexForKey(date) == nil {
                        sections[date] = [item]
                    } else {
                        sections[date]!.append(item)
                    }
                }
            }
            Async.main {
                self.sectionSorting = sections.keys.sort(<)
                self.updateSeries = sections
                self.tableView.reloadData()
            }
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        if SDWebImageManager.sharedManager().isRunning() {
            SDWebImageManager.sharedManager().cancelAll()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        SDImageCache.sharedImageCache().clearMemory()
    }
    
    override func viewDidAppear(animated: Bool) {
      self.showAuth()
    }
    
    func showAuth() {
        guard !Keychaine.sharedInstance.isUserLogin() else { return }
        let srotyBoard = UIStoryboard(name: "Main", bundle: nil)
        let authVc = srotyBoard.instantiateViewControllerWithIdentifier("AuthVC") as! AuthViewController
        authVc.complection = {
            self.getUpdateSeries()
        }
        self.presentViewController(authVc, animated: true, completion: nil)
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        
        ApiClient.sharedInstance.seriesSearch(searchBar.text!, complectionHandler: {[weak self] (series) in
            print(series.count)
            self?.series.removeAll()
            self?.series = series
            self?.search = true
            self?.tableView.reloadData()
            
        }) { (error) in
            
        }
        if self.searchBar.isFirstResponder() {
            if let tapGest = self.tap {
                self.view.removeGestureRecognizer(tapGest)
            }
            self.searchBar.resignFirstResponder()
        }
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        self.view.addGestureRecognizer(self.tap!)
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.characters.count == 0 {
            if self.searchBar.isFirstResponder() {
                self.searchBar.resignFirstResponder()
            }
            self.search = false
            if self.series.count != 0 {
                self.tableView.reloadData()
            }
        }
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if search {
            return self.series.count
        } else {
            return self.updateSeries[self.sectionSorting[section]]!.count
        }
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return search ? 1 : self.updateSeries.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! SeriesSearchCell
        switch search {
        case true:
            if let item: Series = self.series[indexPath.row] {
                cell.setData(item)
            }
        case false:
            if let items: [UpdateModel] = self.updateSeries[self.sectionSorting[indexPath.section]] {
                if let series = items[indexPath.row].series {
                    cell.setData(series)
                } else {
                    //Это костыль, но если бы API сразу отдавала описание серии, его бы не было.
                    ApiClient.sharedInstance.seriesById(items[indexPath.row].id!, complectionHandler: { (series) in
                            items[indexPath.row].series = series
                            cell.setData(series)
                        }, failure: { (error) in
                            print(error.localizedDescription)
                    })
                }
            }
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if search {
            return nil
        } else {
            return self.sectionSorting[section]
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if self.searchBar.isFirstResponder() {
           self.searchBar.resignFirstResponder()
        }
        switch search {
        case true:
            if let item: Series = self.series[indexPath.row] {
                self.performSegueWithIdentifier("showDetail", sender: item.id)
            }
        case false:
            if let items: [UpdateModel] = self.updateSeries[self.sectionSorting[indexPath.section]] {
                if let series = items[indexPath.row].series {
                    self.performSegueWithIdentifier("showDetail", sender: series)
                } else {
                    self.performSegueWithIdentifier("showDetail", sender: items[indexPath.row].id)
                }
            }
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 165
    }
    
    @IBAction func logout(sender: AnyObject) {
        UserOperation().logoutUser()
        Keychaine.sharedInstance.removeUserToken { 
            self.showAuth()
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetail" {
            let vc = segue.destinationViewController as! SeriesDetailViewController
            if let id = sender as? Int {
                vc.seriesById(id)
            } else if let series = sender as? SeriesById {
                vc.showSeries(series)
            }
        }
    }

}
