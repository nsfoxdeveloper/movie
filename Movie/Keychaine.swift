import UIKit
import Locksmith

class Keychaine {
    
    class var sharedInstance: Keychaine {
        struct Static {
            static let instance: Keychaine = Keychaine()
        }
        return Static.instance
    }
    
    // MARK: - Save USER token
    func saveUserToken(token: String, complection:() -> Void) -> Void {
        if let _: String = self.getUserToken() {
            try! Locksmith.updateData(["UserToken": token], forUserAccount: "UserToken")
        } else {
            try! Locksmith.saveData(["UserToken": token], forUserAccount: "UserToken")
        }
        
        complection()
    }
    
    // MARK: - Get User token
    func getUserToken() -> String {
        guard let identrifier = Locksmith.loadDataForUserAccount("UserToken") else {
            return ""
        }
        guard let user = identrifier["UserToken"] as? String else {
            return ""
        }
        return user
    }
    
    func isUserLogin() -> Bool {
        guard let identrifier = Locksmith.loadDataForUserAccount("UserToken") else {
            return false
        }
        guard let _ = identrifier["UserToken"] as? String else {
            return false
        }
        return true
    }
    
    func removeUserToken(complection:() -> Void) {
        guard let identrifier = Locksmith.loadDataForUserAccount("UserToken") else {
            return
        }
        guard let _ = identrifier["UserToken"] as? String else {
            return
        }
        try! Locksmith.deleteDataForUserAccount("UserToken")
        complection()
    }
    
    func headers() -> [String: String] {
        return ["Authorization": "\(self.getUserToken())"]
    }
    
    // MARK: - Save device UUID
    func deviceIdentifier() -> String? {
        if let identrifier = Locksmith.loadDataForUserAccount("UUIDDevice") {
            return identrifier["UUID"] as? String
        } else {
            let UUIDValue = UIDevice.currentDevice().identifierForVendor!.UUIDString
            try! Locksmith.saveData(["UUID": UUIDValue], forUserAccount: "UUIDDevice")
            return UUIDValue
        }
    }
}
