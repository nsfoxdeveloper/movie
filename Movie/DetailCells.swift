//
//  DetailCells.swift
//  Movie
//
//  Created by Юрий Дурнев on 12.07.16.
//  Copyright © 2016 example. All rights reserved.
//

import UIKit
import Eureka
import SKTagView

public class OverviewTableViewCell: Cell<String>, CellType {
    
    @IBOutlet weak var titleLabel: UILabel!
}

public final class OverviewTextRow: Row<String, OverviewTableViewCell>, RowType {
    required public init(tag: String?) {
        super.init(tag: tag)
        displayValueFor = nil
        cellProvider = CellProvider<OverviewTableViewCell>(nibName: "OverviewTableViewCell")
    }
}

public class InfoTableViewCell: Cell<String>, CellType {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
}

public final class InfoTextRow: Row<String, InfoTableViewCell>, RowType {
    required public init(tag: String?) {
        super.init(tag: tag)
        displayValueFor = nil
        cellProvider = CellProvider<InfoTableViewCell>(nibName: "InfoTableViewCell")
    }
}

public class GenreTableViewCell: Cell<String>, CellType {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tagView: SKTagView!
    
    public override func setup() {
        super.setup()
    }
    
    public override func update() {
        super.update()
        self.height = {self.contentView.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize).height + 1}
    }
    
    func setTags(tags: [String]) {
        self.tagView.preferredMaxLayoutWidth = UIScreen.mainScreen().bounds.width
        self.tagView.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        self.tagView.interitemSpacing = 6
        self.tagView.lineSpacing = 6
        self.tagView.removeAllTags()
        
        for tag in tags {
            let sktag = SKTag(text: tag)
            sktag.textColor = UIColor.whiteColor()
            sktag.font = UIFont.systemFontOfSize(15)
            sktag.padding = UIEdgeInsetsMake(8, 8, 8, 8)
            sktag.textColor = UIColor.blackColor()
            sktag.bgColor = UIColor.clearColor()
            sktag.cornerRadius = 0
            sktag.enable = true
            
            self.tagView.addTag(sktag)
        }
        
        self.tagView.didTapTagAtIndex = { (index) in
            print("Для поиска фильмов по жанру")
        }
    }
}

public final class GenreRow: Row<String, GenreTableViewCell>, RowType {
    required public init(tag: String?) {
        super.init(tag: tag)
        displayValueFor = nil
        cellProvider = CellProvider<GenreTableViewCell>(nibName: "GenreTableViewCell")
    }
}


//MARK: - Actors
public class ActorsTableViewCell: Cell<String>, CellType, UICollectionViewDataSource {
    
    var actors = [Actors]() {
        didSet {
            self.collectionView.reloadData()
        }
    }
    
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.registerNib(UINib(nibName: "ActorsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "Cell")
        }
    }
    
    public func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.actors.count
    }
    
    public func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! ActorsCollectionViewCell
        if let actor: Actors = self.actors[indexPath.row] {
            cell.setData(actor)
        }
        return cell
    }
}

public final class ActorsRow: Row<String, ActorsTableViewCell>, RowType {
    required public init(tag: String?) {
        super.init(tag: tag)
        displayValueFor = nil
        cellProvider = CellProvider<ActorsTableViewCell>(nibName: "ActorsTableViewCell")
    }
}

class ActorsCollectionViewCell: UICollectionViewCell {
     @IBOutlet weak var actorName: UILabel!
     @IBOutlet weak var actorRole: UILabel!
     @IBOutlet weak var actorImage: UIImageView!
    
    func setData(actor: Actors) {
        if let name = actor.name {
            self.actorName.text = name
        }
        if let role = actor.role {
            self.actorRole.text = role
        }
        if let url = actor.image {
            let URL = NSURL(string: "http://thetvdb.com/banners/\(url)")!
            self.actorImage.sd_setImageWithURL(URL)
        }
    }
}

//MARK: - Seasons

public class SeasonsTableViewCell: Cell<String>, CellType, UICollectionViewDataSource, UICollectionViewDelegate {
    
    var seasonIndex: ((index: Int) -> Void)?
    
    @IBOutlet weak var allEpisodes: UILabel!
    var seasons = [Int]() {
        didSet {
            self.collectionView.reloadData()
        }
    }
    
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.registerNib(UINib(nibName: "SeasonsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "Cell")
        }
    }
    
    public func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.seasons.count
    }
    
    public func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! SeasonsCollectionViewCell
        cell.seasonLabel.text = "\(self.seasons[indexPath.row])"
        return cell
    }
    
    public func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if let index: Int = self.seasons[indexPath.row] {
            self.seasonIndex?(index: index)
        }
    }
}

public final class SeasonsRow: Row<String, SeasonsTableViewCell>, RowType {
    required public init(tag: String?) {
        super.init(tag: tag)
        displayValueFor = nil
        cellProvider = CellProvider<SeasonsTableViewCell>(nibName: "SeasonsTableViewCell")
    }
}

class SeasonsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var seasonLabel: UILabel!
    
    override func layoutSubviews() {
        seasonLabel.layer.masksToBounds = true
        seasonLabel.layer.cornerRadius = seasonLabel.frame.width / 2
    }
}


