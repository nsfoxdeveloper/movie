//
//  AuthViewController.swift
//  Movie
//
//  Created by Юрий Дурнев on 05.07.16.
//  Copyright © 2016 example. All rights reserved.
//

import UIKit
import VK_ios_sdk
import FBSDKCoreKit
import FBSDKLoginKit
import TwitterKit

class AuthViewController: UIViewController, VKSdkDelegate, VKSdkUIDelegate {
    var complection: (() -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        let sdkInstance = VKSdk.initializeWithAppId("5535157")
        sdkInstance.registerDelegate(self)
        sdkInstance.uiDelegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func fbAuthButton(sender: AnyObject) {
        
        FBSDKLoginManager().logInWithReadPermissions(["public_profile", "email"], fromViewController: self) {(result, error) in
            if error != nil {
                
            } else if (result.isCancelled) {
                
            } else {
                ApiClient.sharedInstance.auth(result.token.userID, complectionHandler: {
                    self.dismissViewControllerAnimated(true, completion: { 
                        self.complection
                    })
                    }, failure: { (error) in
                })
            }
        }
    }
    
    @IBAction func vkAuthButton(sender: AnyObject) {
        if VKSdk.isLoggedIn() {
            ApiClient.sharedInstance.auth(VKSdk.accessToken().userId, complectionHandler: {
                self.dismissViewControllerAnimated(true, completion: {
                    self.complection
                })
                }, failure: { (error) in
            })
        } else {
            VKSdk.authorize([VK_PER_EMAIL])
        }
    }
    
    @IBAction func twAuthButton(sender: AnyObject) {
        Twitter.sharedInstance().logInWithCompletion {(session, error) in
            if (session != nil) {
                ApiClient.sharedInstance.auth(session!.userID, complectionHandler: {
                    self.dismissViewControllerAnimated(true, completion: {
                        self.complection?()
                    })
                    }, failure: { (error) in
                })
            } else {
                print("error: \(error!.localizedDescription)");
            }
        }
    }
    
    // MARK: VK delegate
    func vkSdkAccessAuthorizationFinishedWithResult(result: VKAuthorizationResult!) {
        guard result.token != nil else {
            return
        }
        
        guard let userID = result.token.userId else {
            return
        }
        ApiClient.sharedInstance.auth(userID, complectionHandler: {
            self.dismissViewControllerAnimated(true, completion: {
                self.complection
            })
            }, failure: { (error) in
        })
    }
    
    func vkSdkUserAuthorizationFailed() { }
    
    // MARK: - VK UI delegate
    
    func vkSdkShouldPresentViewController(controller: UIViewController!) {
        presentViewController(controller, animated: true, completion: nil)
    }
    
    func vkSdkNeedCaptchaEnter(captchaError: VKError!) {
    }
    

}

