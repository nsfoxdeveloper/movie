//
//  EpisodeDetailViewController.swift
//  Movie
//
//  Created by Юрий Дурнев on 13.07.16.
//  Copyright © 2016 example. All rights reserved.
//

import UIKit
import Eureka

class EpisodeDetailViewController: SeriesDetailViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func seriesById(id: Int) {
        ApiClient.sharedInstance.episodesById(id, complectionHandler: {[weak self] (episodes) in
                self?.showEpisodes(episodes)
            }) { (error) in
                print(error.localizedDescription)
        }
    }
    
    func showEpisodes(episodes: Episodes) {
        form +++ Section()
        if let url = episodes.filename {
            let URL = NSURL(string: "http://thetvdb.com/banners/\(url)")!
            self.headerView.headerImage.sd_setImageWithURL(URL, placeholderImage: UIImage(named: "placeholder-search"))
        }
        if let number = episodes.airedEpisodeNumber {
            self.addinfoRow("Aired Episode Number", detail: "\(number)")
        }
        if let overview = episodes.overview {
            self.addOverviewRow(overview)
        }
        if let title = episodes.episodeName {
            self.title = title
        }
        if let firstAired = episodes.firstAired {
            self.addinfoRow("First Aired", detail: firstAired.toString(format: .Custom("MMMM d, YYYY")))
        }
        if let guest = episodes.guestStars where !guest.isEmpty {
            self.addGenreRow(guest, title: "Guest Stars:")
        }
        if let director = episodes.director where director != "" {
            self.addinfoRow("Director", detail: director)
        }
        if let writer = episodes.writers where !writer.isEmpty {
            self.addGenreRow(writer, title: "Writer:")
        }
        if let imdbid = episodes.imdbId where imdbid != "" {
            self.addinfoRow("IMDB.com ID", detail: imdbid)
        }
    }
}
