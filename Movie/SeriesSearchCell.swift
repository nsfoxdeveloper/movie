//
//  SeriesSearchCell.swift
//  Movie
//
//  Created by Юрий Дурнев on 06.07.16.
//  Copyright © 2016 example. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import Async

class SeriesSearchCell: UITableViewCell {

    @IBOutlet weak var seriesImage: UIImageView!
    @IBOutlet weak var seriesTitle: UILabel!
    @IBOutlet weak var seriesOverview: UILabel!
    @IBOutlet weak var blurImage: UIImageView!
    
    lazy var blurEffectView: UIVisualEffectView = { [unowned self] in
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.blurImage!.bounds
        blurEffectView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        return blurEffectView
    }()
    
    func setData(series: Series) {
        Async.background {
            if let title = series.seriesName {
                Async.main {
                   self.seriesTitle.text = title
                }
            } else {
                Async.main {
                    self.seriesTitle.text = "Нет описания"
                }
            }
            
            if let overview = series.overview {
                Async.main {
                    self.seriesOverview.text = overview
                }
            } else if let network = series.network {
                Async.main {
                   self.seriesOverview.text = network
                }
            }
            
            if let banner = series.banner where banner != "" {
                let URL = NSURL(string: "http://thetvdb.com/banners/\(banner)")!
                self.seriesImage.sd_setImageWithURL(URL, placeholderImage: UIImage(named: "placeholder-search"), completed: {[weak self] (image, error, _, _) in
                    if image != nil {
                        Async.main {
                            self?.seriesImage.image = image
                            self?.blurImage.image = image
                            self?.blurImage?.addSubview(self!.blurEffectView)
                        }
                    }
                    })
            }
        }
       
    }
    
    override func prepareForReuse() {
        self.seriesImage.image = UIImage(named: "placeholder-search")
        self.seriesTitle.text = nil
        self.seriesOverview.text = nil
    }
}
