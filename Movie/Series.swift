//
//  Series.swift
//  Movie
//
//  Created by Юрий Дурнев on 06.07.16.
//  Copyright © 2016 example. All rights reserved.
//

import Foundation
import ObjectMapper
import AFDateHelper

class DataArray<T: Mappable>: Mappable {
    var data: [T]?
    required init?(_ map: Map) {}
   
    func mapping(map: Map) {
        data <- map["data"]
    }
}

class DataObject<T: Mappable>: Mappable {
    var data: T?
    required init?(_ map: Map) {}
    
    func mapping(map: Map) {
        data <- map["data"]
    }
}

class Series: Mappable {
    
    var banner: String?
    var firstAired: NSDate?
    var id: Int?
    var network: String?
    var overview: String?
    var seriesName: String?
    var status: String?
    
    required init?(_ map: Map) {}
    
    func mapping(map: Map) {
        banner <- map["banner"]
        firstAired <- (map["firstAired"], DateTransform())
        id <- map["id"]
        network <- map["network"]
        overview <- map["overview"]
        seriesName <- map["seriesName"]
        status <- map["status"]
    }
}

class SeriesById: Series {

    var networkId: String?
    var runtime: String?
    var genre: [String]?
    var lastUpdated: NSDate?
    var airsDayOfWeek: String?
    var airsTime: String?
    var rating: String?
    var imdbId: String?
    var zap2itId: String?
    var added: NSDate?
    var addedBy: Int?
    var siteRating: Int?
    var siteRatingCount: Int?
    
    
    override func mapping(map: Map) {
        banner <- map["banner"]
        firstAired <- (map["firstAired"], DateTransform())
        id <- map["id"]
        network <- map["network"]
        overview <- map["overview"]
        seriesName <- map["seriesName"]
        status <- map["status"]
        networkId <- map["networkId"]
        runtime <- map["runtime"]
        genre <- map["genre"]
        lastUpdated <- (map["lastUpdated"], DateTransform())
        airsDayOfWeek <- map["airsDayOfWeek"]
        airsTime <- map["airsTime"]
        rating <- map["rating"]
        imdbId <- map["imdbId"]
        zap2itId <- map["zap2itId"]
        added <- (map["added"], DateTransform())
        addedBy <- map["addedBy"]
        siteRating <- map["siteRating"]
        siteRatingCount <- map["siteRatingCount"]
    }
}

class UpdateModel: Mappable {
    
    var lastUpdated: NSDate?
    var id: Int?
    var series: SeriesById?
    
    required init?(_ map: Map) {}
    
    func mapping(map: Map) {
        lastUpdated <- (map["lastUpdated"], DateTransform())
        id <- map["id"]
    }
}

class Actors: Mappable {
    
    var seriesId: Int?
    var id: Int?
    var name: String?
    var role: String?
    var sortOrder: Int?
    var image: String?
    var imageAuthor: Int?
    var imageAdded: NSDate?
    var lastUpdated: NSDate?
    
    required init?(_ map: Map) {}
    
    func mapping(map: Map) {
        lastUpdated <- (map["lastUpdated"], DateTransform())
        id <- map["id"]
        name <- map["name"]
        role <- map["role"]
        sortOrder <- map["sortOrder"]
        image <- map["image"]
        imageAuthor <- map["imageAuthor"]
        imageAdded <- (map["imageAdded"], DateTransform())
    }
}

class SummarySeasons: Mappable {
    
    var airedSeasons: [String]?
    var airedEpisodes: String?
    
    required init?(_ map: Map) {}
    
    func mapping(map: Map) {
        airedSeasons <- map["airedSeasons"]
        airedEpisodes <- map["airedEpisodes"]
    }
}

class AllEpisodes: Mappable {
    
    var absoluteNumber: Int?
    var airedEpisodeNumber: Int?
    var airedSeason: Int?
    var airedSeasonID: Int?
    var dvdEpisodeNumber: Int?
    var dvdSeason: Int?
    var episodeName: String?
    var firstAired: NSDate?
    var id: Int?
    var overview: String?
    
    required init?(_ map: Map) {}
    
    func mapping(map: Map) {
        absoluteNumber <- map["absoluteNumber"]
        airedEpisodeNumber <- map["airedEpisodeNumber"]
        airedSeason <- map["airedSeason"]
        airedSeasonID <- map["airedSeasonID"]
        dvdEpisodeNumber <- map["dvdEpisodeNumber"]
        dvdSeason <- map["dvdSeason"]
        episodeName <- map["episodeName"]
        firstAired <- (map["firstAired"], DateTransform())
        id <- map["id"]
        overview <- map["overview"]
    }
}

class Episodes: AllEpisodes {
    
    var guestStars: [String]?
    var director: String?
    var writers: [String]?
    var filename: String?
    var seriesId: Int?
    var imdbId: String?
    var siteRating: Int?
    var siteRatingCount: Int?
    
    override func mapping(map: Map) {
        absoluteNumber <- map["absoluteNumber"]
        airedEpisodeNumber <- map["airedEpisodeNumber"]
        airedSeason <- map["airedSeason"]
        airedSeasonID <- map["airedSeasonID"]
        dvdEpisodeNumber <- map["dvdEpisodeNumber"]
        dvdSeason <- map["dvdSeason"]
        episodeName <- map["episodeName"]
        firstAired <- (map["firstAired"], DateTransform())
        id <- map["id"]
        overview <- map["overview"]
        guestStars <- map["guestStars"]
        director <- map["director"]
        writers <- map["writers"]
        filename <- map["filename"]
        seriesId <- map["seriesId"]
        imdbId <- map["imdbId"]
        siteRating <- map["siteRating"]
        siteRatingCount <- map["siteRatingCount"]
    }
}


public class DateTransform: TransformType {
    public typealias Object = NSDate
    public typealias JSON = Double
    
    public init() {}
    
    public func transformFromJSON(value: AnyObject?) -> NSDate? {
        if let timeInt = value as? Double {
            return NSDate(timeIntervalSince1970: NSTimeInterval(timeInt))
        }
        
        if let timeStr = value as? String {
            return NSDate(fromString: timeStr, format: .ISO8601(ISO8601Format.Date))
        }
        return nil
    }
    
    public func transformToJSON(value: NSDate?) -> Double? {
        if let date = value {
            return Double(date.timeIntervalSince1970)
        }
        return nil
    }
}


