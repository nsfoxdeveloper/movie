//
//  AllEpisodesViewController.swift
//  Movie
//
//  Created by Юрий Дурнев on 13.07.16.
//  Copyright © 2016 example. All rights reserved.
//

import UIKit

class AllEpisodesViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.estimatedRowHeight = 84
            tableView.rowHeight = UITableViewAutomaticDimension
        }
    }
    
    var allEpisodes = [AllEpisodes]() {
        didSet {
            self.tableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func allEpisodes(seasonId: Int, seasonIndex: Int ) {
        self.title = "Season \(seasonIndex)"
        ApiClient.sharedInstance.seasonEpisodes(seasonId, seasonIndex: seasonIndex, complectionHandler: { (episodes) in
            if !episodes.isEmpty {
                self.allEpisodes = episodes
            }
            }) { (error) in
                print(error.localizedDescription)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showEpisodes" {
            let vc = segue.destinationViewController as! EpisodeDetailViewController
            if let episodeId = sender as? Int {
                vc.seriesById(episodeId)
            }
        }
    }
}

extension AllEpisodesViewController: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allEpisodes.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! AllEpisodesCell
        if let episodes: AllEpisodes = self.allEpisodes[indexPath.row] {
            cell.setData(episodes)
        }
        return cell
    }
}

extension AllEpisodesViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if let episodes: AllEpisodes = self.allEpisodes[indexPath.row] {
            self.performSegueWithIdentifier("showEpisodes", sender: episodes.id)
        }
    }
}
