//
//  ApiClient.swift
//  Movie
//
//  Created by Юрий Дурнев on 06.07.16.
//  Copyright © 2016 example. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import RealmSwift

enum Router: URLRequestConvertible {
    
    static let baseUrl = "https://api.thetvdb.com"
    static let apiKey = "931504848C306266"
    
    case UserAuth
    case RefreshToken
    case Series(name: String)
    case SeriesById(id: Int)
    case Update
    case Actors(id: Int)
    case Summary(id: Int)
    case EpisodesQuery(id: Int, seasonIndex: Int)
    case Episodes(id: Int)
    
    var URLRequest: NSMutableURLRequest {
        
        var method: Alamofire.Method {
            switch self {
            case .UserAuth:
                return .POST
            case .RefreshToken:
                return .GET
            case .Series:
                return .GET
            case .SeriesById:
                return .GET
            case .Update:
                return .GET
            case .Actors:
                return .GET
            case .Summary:
                return .GET
            case .EpisodesQuery:
                return .GET
            case .Episodes:
                return .GET
            }
            
        }
        
        var path: String {
            switch self {
            case .UserAuth:
                return "/login"
            case .RefreshToken:
                return "/refresh_token"
            case .Series:
                return "/search/series"
            case .SeriesById(let id):
                return "/series/\(id)"
            case .Update:
                return "/updated/query"
            case .Actors(let id):
                return "/series/\(id)/actors"
            case .Summary(let id):
                return "/series/\(id)/episodes/summary"
            case .EpisodesQuery(let id, _):
                return "/series/\(id)/episodes/query"
            case .Episodes(let id):
                return "/episodes/\(id)"
            }
        }
        
        var parameters: [String: AnyObject]? {
            switch self {
            case .UserAuth:
                return ["username": "Nsfox", "userkey": "283074E66CB91450", "apikey": "931504848C306266"]
            case .RefreshToken:
                return nil
            case .Series(let name):
                return self.searchSeries(name)
            case .SeriesById:
                return nil
            case .Update:
                return ["fromTime": NSDate().dateAtStartOfDay().timeIntervalSince1970]
            case .Actors:
                return nil
            case .Summary:
                return nil
            case .EpisodesQuery(_, let seasonIndex):
                return ["airedSeason": seasonIndex]
            case .Episodes:
                return nil
            }
        }
        
        let URL = NSURL(string: Router.baseUrl)
        let URLRequest = NSMutableURLRequest(URL: URL!.URLByAppendingPathComponent(path))
        let isUser = Keychaine.sharedInstance.isUserLogin()
        let encoding = isUser ? Alamofire.ParameterEncoding.URL : Alamofire.ParameterEncoding.JSON
        URLRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        URLRequest.addValue("en", forHTTPHeaderField: "Accept-Language")
        let key = isUser ? Keychaine.sharedInstance.getUserToken() : Router.apiKey
        URLRequest.addValue("Bearer \(key)", forHTTPHeaderField: "Authorization")
        URLRequest.HTTPMethod = method.rawValue
        URLRequest.timeoutInterval = NSTimeInterval(10 * 1000)
        
        return encoding.encode(URLRequest, parameters: parameters).0
    }
    
    func searchSeries(name: String) -> [String: AnyObject]? {
        if let _ = Int(name) {
            if name.characters.count == 7 {
                return ["imdbId": "tt\(name)"]
            } else if name.characters.count == 8 {
                return ["zap2itId": "EP\(name)"]
            }
        } else {
            return ["name": name]
        }
        return nil
    }
}

enum UserError: ErrorType {
    case IncorrectToken
}

class ApiClient {
        
    class var sharedInstance: ApiClient {
        struct Static {
            static let instance: ApiClient = ApiClient()
        }
        return Static.instance
    }
    
    func auth(userId: String, complectionHandler:(() -> Void)?, failure:((error: ErrorType) -> Void)?) {
        Alamofire.request(Router.UserAuth).responseJSON { (response) in
            switch response.result {
            case .Success:
                guard let response = response.result.value as? [String: AnyObject] else {
                    failure?(error: UserError.IncorrectToken)
                    return
                }
                guard let token = response["token"] as? String else {
                    failure?(error: UserError.IncorrectToken)
                    return
                }
                Keychaine.sharedInstance.saveUserToken(token, complection: {
                    do {
                        let realm = try Realm()
                        try realm.write {
                            realm.create(User.self, value: ["userId": userId, "isLogin": true], update: true)
                        }
                       
                    } catch let error as NSError {
                        print(error.localizedDescription)
                    }

                    complectionHandler?()
                    print("Save User token: \(token)")
                })
            case .Failure(let error): failure?(error: error)
            }
        }
    }
    
    func seriesSearch(name: String, complectionHandler:((series: [Series]) -> Void)?, failure:((error: NSError) -> Void)?) {
        Alamofire.request(Router.Series(name: name)).responseObject { (response: Response<DataArray<Series>, NSError>) in
            switch response.result {
            case .Success:
                if let series = response.result.value {
                    print(series)
                    if let array = series.data where !array.isEmpty {
                       complectionHandler?(series: array)
                    }
                }
            case .Failure(let error):
                print(error.localizedDescription)
                failure?(error: error)
            }
        }
    }
    
    func seriesById(id: Int, complectionHandler:((series: SeriesById) -> Void)?, failure:((error: NSError) -> Void)?) {
        Alamofire.request(Router.SeriesById(id: id)).responseObject { (response: Response<DataObject<SeriesById>, NSError>) in
            switch response.result {
            case .Success:
                if let data = response.result.value {
                    if let series = data.data {
                        complectionHandler?(series: series)
                    }
                }
            case .Failure(let error): failure?(error: error)
            }
        }
    }
    
    func seriesUpdate(complectionHandler:((update: [UpdateModel]) -> Void)?, failure:((error: NSError) -> Void)?) {
        Alamofire.request(Router.Update).responseObject { (response: Response<DataArray<UpdateModel>, NSError>) in
            switch response.result {
            case .Success:
                if let series = response.result.value {
                    if let array = series.data where !array.isEmpty {
                        complectionHandler?(update: array)
                    }
                }
            case .Failure(let error):
                print(error.localizedDescription)
                failure?(error: error)
            }
        }
    }
    
    func actors(id: Int,complectionHandler:((actors: [Actors]) -> Void)?, failure:((error: NSError) -> Void)?) {
        Alamofire.request(Router.Actors(id: id)).responseObject { (response: Response<DataArray<Actors>, NSError>) in
            switch response.result {
            case .Success:
                if let series = response.result.value {
                    if let array = series.data where !array.isEmpty {
                        complectionHandler?(actors: array)
                    }
                }
            case .Failure(let error): failure?(error: error)
            }
        }
    }
    
    func seasonSummary(id: Int, complectionHandler:((seasons: SummarySeasons) -> Void)?, failure:((error: NSError) -> Void)?) {
        Alamofire.request(Router.Summary(id: id)).responseObject { (response: Response<DataObject<SummarySeasons>, NSError>) in
            switch response.result {
            case .Success:
                if let data = response.result.value {
                    if let seasons = data.data {
                        complectionHandler?(seasons: seasons)
                    }
                }
            case .Failure(let error): failure?(error: error)
            }
        }
    }
    
    func seasonEpisodes(id: Int, seasonIndex: Int, complectionHandler:((episodes: [AllEpisodes]) -> Void)?, failure:((error: NSError) -> Void)?) {
        Alamofire.request(Router.EpisodesQuery(id: id, seasonIndex: seasonIndex)).responseObject { (response: Response<DataArray<AllEpisodes>, NSError>) in
            switch response.result {
            case .Success:
                if let response = response.result.value {
                    if let array = response.data where !array.isEmpty {
                        complectionHandler?(episodes: array)
                    }
                }
            case .Failure(let error): failure?(error: error)
            }
        }
    }
    
    func episodesById(id: Int, complectionHandler:((episodes: Episodes) -> Void)?, failure:((error: NSError) -> Void)?) {
        Alamofire.request(Router.Episodes(id: id)).responseObject { (response: Response<DataObject<Episodes>, NSError>) in
            switch response.result {
            case .Success:
                if let data = response.result.value {
                    if let episodes = data.data {
                        complectionHandler?(episodes: episodes)
                    }
                }
            case .Failure(let error): failure?(error: error)
            }
        }
    }
}

