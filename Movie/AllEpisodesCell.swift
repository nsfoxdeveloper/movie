//
//  AllEpisodesCell.swift
//  Movie
//
//  Created by Юрий Дурнев on 13.07.16.
//  Copyright © 2016 example. All rights reserved.
//

import UIKit

class AllEpisodesCell: UITableViewCell {

    @IBOutlet weak var episodesNumber: UILabel!
    @IBOutlet weak var episodesName: UILabel!
    @IBOutlet weak var episodesOverview: UILabel!
   
    func setData(episodes: AllEpisodes) {
        if let number = episodes.airedEpisodeNumber {
            self.episodesNumber.text = "\(number)"
        }
        if let name = episodes.episodeName {
            self.episodesName.text = name
        }
        if let overview = episodes.overview {
            self.episodesOverview.text = overview
        }
    }

}
