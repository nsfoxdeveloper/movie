//
//  SeriesDetailViewController.swift
//  Movie
//
//  Created by Юрий Дурнев on 07.07.16.
//  Copyright © 2016 example. All rights reserved.
//

import UIKit
import Eureka
import Async
import Alamofire
import RealmSwift

class SeriesDetailViewController: FormViewController {
    
    var favButton: UIBarButtonItem?
    var series: SeriesById? {
        didSet {
            self.checkFav({ (_) in
                    self.favButton?.image = UIImage(named: "ic-favorite-app-bar-pressed")
                }) { 
                    self.favButton?.image = UIImage(named: "ic-favorite-app-bar")
            }
        }
    }
   
    lazy var headerView: DetailHeaderView = {
        return NSBundle.mainBundle().loadNibNamed("DetailHeaderView", owner: self, options: nil).first! as! DetailHeaderView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.contentAnchor = .Bottom
        tableView?.addSubview(headerView)
    }
    
    func seriesById(id: Int) {
        ApiClient.sharedInstance.seriesById(id, complectionHandler: {[weak self] (series) in
            self?.showSeries(series)
            }) { (error) in
        }
    }
    
    func showSeries(series: SeriesById) {
        favButton = UIBarButtonItem(image: UIImage(named: "ic-favorite-app-bar"), style: .Plain, target: self, action: #selector(self.addToFavorites(_:)))
        self.navigationItem.rightBarButtonItem = favButton
        self.series = series
        self.title = series.seriesName
        self.headerView.setData(series)
        form +++ Section()
        
        if let overview = series.overview {
            self.addOverviewRow(overview)
        }
        if let firstAired = series.firstAired {
            self.addinfoRow("First Aired", detail: firstAired.toString(format: .Custom("MMMM d, YYYY")))
        }
        if let airDay = series.airsDayOfWeek {
            self.addinfoRow("Air Day", detail: airDay)
        }
        if let time = series.airsTime {
            self.addinfoRow("Air Time", detail: time)
        }
        if let runtime = series.runtime {
            self.addinfoRow("Runtime", detail: runtime)
        }
        if let network = series.network {
            self.addinfoRow("Network", detail: network)
        }
        if let status = series.status {
            self.addinfoRow("Status", detail: status)
        }
        if let genre = series.genre {
            self.addGenreRow(genre)
        }
        self.addActors(series.id!)
        self.addSeasons(series.id!)
    }
    
    func addinfoRow(title: String, detail: String) {
        form.last! <<< InfoTextRow().cellSetup({ (cell, row) in
            cell.titleLabel.text = title
            cell.detailLabel.text = detail
        })
    }
    
    func addGenreRow(ganre: [String], title: String = "Genre") {
        form.last! <<< GenreRow().cellSetup({ (cell, row) in
            cell.setTags(ganre)
            cell.titleLabel.text = title
        })
    }
    
    func addOverviewRow(title: String) {
        form.last! <<< OverviewTextRow().cellSetup({ (cell, row) in
            cell.titleLabel.text = title
        })
    }
    
    func addActors(id: Int) {
        ApiClient.sharedInstance.actors(id, complectionHandler: { (actors) in
            if actors.count != 0 {
                self.form.last! <<< ActorsRow().cellSetup({ (cell, row) in
                    cell.actors = actors
                    cell.height = { 278 }
                })
            }
            }, failure: nil)
    }
    
    func addSeasons(id: Int) {
        ApiClient.sharedInstance.seasonSummary(id, complectionHandler: {[weak self] (seasons) in
            if seasons.airedSeasons?.count != 0 {
                if let strongSelf = self {
                    strongSelf.form.last! <<< SeasonsRow().cellSetup({ (cell, row) in
                        var sort = [Int]()
                        for item in seasons.airedSeasons! {
                            sort.append(Int(item)!)
                        }
                        sort = sort.sort(<)
                        cell.seasons = sort
                        cell.allEpisodes.text = seasons.airedEpisodes!
                        cell.height = { 120 }
                        cell.seasonIndex = { [weak self] (index) in
                            self?.performSegueWithIdentifier("allEpisodes", sender: index)
                        }
                    })
                }
            }
            
            }, failure: nil)
    }
    
    
    func checkFav(find: ((index: Int) -> Void)?, noFav:(() -> Void)?) {
        guard let id = self.series?.id else { return }
        if let list = UserOperation().user()?.favoritesList {
            let favorites = list.filter("favId == \(id)")
            if favorites.count == 0 {
                noFav?()
            } else {
                if let favIndex = list.indexOf("favId == \(id)") {
                    find?(index: favIndex)
                }
            }
        }
    }
    
    func addToFavorites(sender: AnyObject) {
        self.checkFav({ (index) in
                UserOperation().userOperation({ (user) in
                    let fav = user?.favoritesList[index]
                    try! Realm().delete(fav!)
                })
                self.favButton?.image = UIImage(named: "ic-favorite-app-bar")
            }) {
                UserOperation().userOperation({ (user) in
                    let fav = Favorites()
                    fav.favId = self.series!.id!
                    fav.objectString = self.series?.toJSONString()
                    user!.favoritesList.append(fav)
                })
                self.favButton?.image = UIImage(named: "ic-favorite-app-bar-pressed")
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "allEpisodes" {
            let vc = segue.destinationViewController as! AllEpisodesViewController
            if let id = sender as? Int {
                vc.allEpisodes(self.series!.id!, seasonIndex: id)
            }
        }
    }
    
}
